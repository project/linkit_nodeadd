<?php

$nodepluginclass = ctools_plugin_load_class('linkit', 'linkit_plugins', 'entity:node', 'handler');
if ($nodepluginclass == 'LinkitPluginNode') {
  $plugin = array(
    'label' => t('Node Create'),
    'handler' => array(
      'class' => 'LinkitPluginNodeAdd',
      'file' => 'linkit-plugin-nodeadd.class.php',
    ),
  );
}
