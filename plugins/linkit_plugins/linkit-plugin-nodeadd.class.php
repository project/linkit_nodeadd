<?php


class LinkitPluginNodeAdd extends LinkitPluginNode {
  function ui_title() {
    return t('Node Add');
  }

  function ui_description() {
    return t('Link to node add form with prepopulated title.');
  }

  function __construct($plugin, $profile) {
    $plugin['entity_type'] = 'node';
    parent::__construct($plugin, $profile);
    // Only filter on unpublished content if the user has access to see them
    $this->conf['include_unpublished'] = (user_access('administer nodes') || user_access('bypass node access'));
  }

  /**
   * Return node-creation link if there is no node with that title
   */
  function autocomplete_callback() {
    // Get query instance with entity type filter and published node filter
    if ($this->conf['exclude_existing']) {
      parent::getQueryInstance();
      $query =& $this->query;

      $query->addTag('node_access');

      if (isset($this->entity_key_bundle) && isset($this->conf['bundles']) ) {
        if ($bundles = array_filter($this->conf['bundles'])) {
          $query->propertyCondition($this->entity_key_bundle, $bundles, 'IN');
        }
      }

      $query->propertyCondition('title', $this->search_string);
    }

    $result = isset($query) ? $query->count()->execute() : 0;

    $return = array();
    if (!$result) {
      // build URL options
      $options = array(
        'query' => array(
          'edit[title]' => $this->search_string,
          'redirect_if_exists' => TRUE,
        ),
      );

      // build BAC row
      // TODO: figure out how to add a class to "create" links but
      // be able to remove it later if the page is created. Best guess
      // right now is some JS??

      if (empty($bundles)) {
        $bundles = $this->entity_info['bundles'];
      }

      foreach ($bundles as $type => $bundle) {
        $t_opts = array(
          '@type' => $bundle['label'],
          '@title' => $this->search_string,
        );

        $return[] = array(
          'title' => t('@title', $t_opts),
          'description' => t('Link to "Create @type" form', $t_opts),
          'path' => url('node/add/' . $type, $options),
          'group' => t('Create new node'),
        );
      }
    }
    dpm($return, 'return');
    return $return;
  }

  /**
   * Generate settings form
   */
  function buildSettingsForm() {
    $node_form = parent::buildSettingsForm();
    $node_form = $node_form[$this->plugin['name']];

    // copy fieldset definition from parent
    foreach($node_form as $key => $value) {
      if (strpos($key, '#') === 0) {
        $form[$key] = $value;
      }
    }

    $form['exclude_existing'] = array(
      '#title' => t('Exclude titles corresponding to existing nodes'),
      '#type' => 'checkbox',
      '#default_value' => isset($this->conf['exclude_existing']) ? $this->conf['exclude_existing'] : 1,
    );

    ctools_include('dependent');
    $form['bundles'] = $node_form['bundles'];
    $form['bundles']['#dependency'] = array('edit-data-nodecreate-exclude-existing' => array(1));
    $form['bundles']['#title'] = t('Available types');
    $form['bundles']['#description'] = t('If left blank, all types to which the user has create access will be available.');

    return array($this->plugin['name'] => $form);
  }
}
