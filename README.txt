Linkit Node Add
========

Extend LinkIt to enable users to create links to nodes that don't exist
at the time of linking. Such links go directly to the node/add/<type> 
form. If a node with that title is created later, those links will be
rewritten and redirected to that node.

Maintainers
========

Evan Heidtmann, Squishymedia. 

Development supported by
--------

Squishymedia: http://squishymedia.com
